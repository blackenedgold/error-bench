use failure::Error as FailureError;
use std::error::Error as StdError;
use std::fmt;

#[derive(Debug)]
struct MyError;

impl fmt::Display for MyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "My Error")
    }
}

impl StdError for MyError {}

use bencher::{benchmark_group, benchmark_main, Bencher};

fn bench_error(b: &mut Bencher) {
    b.iter(|| -> Box<dyn StdError> { Box::new(MyError) })
}

fn bench_fail(b: &mut Bencher) {
    b.iter(|| -> FailureError { MyError.into() })
}

fn deep_stack_call<T>(n: u32, f: impl FnOnce() -> T) -> T {
    if n == 0 {
        f()
    } else {
        deep_stack_call(n - 1, f)
    }
}

fn bench_error_stack(b: &mut Bencher) {
    b.iter(|| -> Box<dyn StdError> { deep_stack_call(100, || Box::new(MyError)) })
}

fn bench_fail_stack(b: &mut Bencher) {
    b.iter(|| -> FailureError { deep_stack_call(100, || MyError.into()) })
}

benchmark_group!(benches, bench_error, bench_fail);
benchmark_group!(deep_stack_bench, bench_error_stack, bench_fail_stack);
benchmark_main!(benches, deep_stack_bench);
